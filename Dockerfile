FROM ubuntu:jammy AS builder

ENV PATH "$PATH:/work/cargo/bin"
WORKDIR /work

RUN apt update && \
    apt upgrade -y && \
    apt -y install wget curl build-essential cmake python3-dev musl-dev python3-pip
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs/ -o rustup.sh && \
    chmod +x rustup.sh
RUN CARGO_HOME=/work/cargo ./rustup.sh -y --profile default --default-toolchain stable --default-host aarch64-unknown-linux-gnu
# RUN source "/work/cargo/env"
RUN /work/cargo/bin/cargo --version
RUN curl https://server.duinocoin.com/fasthash/libducohash.tar.gz -o libducohash.tar.gz
RUN tar -xvf libducohash.tar.gz
RUN cd libducohash && /work/cargo/bin/cargo build --release

FROM ubuntu:jammy
WORKDIR /app

ENV VERSION "3.4"
ENV USERNAME "helix77"
ENV MINING_KEY "None"
ENV INTENSITY "25"
ENV THREADS "4"
ENV DIFFICULTY "MEDIUM"
ENV ALGORITHM "fasthash"
ENV DONATE "0"
ENV RIG "TuringPi2-Cluster"
ENV LANGUAGE "english"
ENV SOC_TIMEOUT "20"
ENV REPORT_SEC "300"
ENV RASPI_LEDS "n"
ENV RASPI_CPU_IOT "y"
ENV DISCORD_RP "n"
#ENV PATH "$PATH:$HOME/.cargo/bin"

COPY run.sh /

RUN apt-get update && \
  apt-get upgrade -qqy && \
  apt-get -qqy install \
    python3-pip \
    git \
    gcc \
    musl-dev \
    python3-dev \
  && rm -rf /var/lib/apt/lists/*

# Installing Duino Coin Miner
RUN /bin/sh -c python3 -m pip install requests colorama py-cpuinfo psutil pypresence && \
  git clone https://github.com/revoxhere/duino-coin --branch ${VERSION} ${HOME}/app && \
  mkdir -p "${HOME}/app/Duino-Coin PC Miner ${VERSION}" && \
  touch ${HOME}/app/Duino-Coin\ PC\ Miner\ ${VERSION}/Settings.cfg && \
  chmod a+x /run.sh
COPY --from=builder /work/libducohash/target/release/libducohasher.so /root/app/libducohasher.so

CMD [ "/run.sh"]
